// ==UserScript==
// @name         TiQ Vendor Documentation Button
// @author       Alex Vo
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Inserts a button above the "Tag Vendor Information" description linking to Tealium Documentation.
// @match        https://sso.tealiumiq.com/tms*
// @exclude      https://sso.tealiumiq.com/tms*?*account=*
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function() {
    'use strict';

    // Lookup table for vendor documentation stored in Google Sheets
    const spreadsheetUrl = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTNuK070FmJU4fpZYoUMzOjuB2byoIW9RHoTr2YvLQQyrbtK5msJku1QRJy2udZUetgdtQNgVnflXbl/pub?gid=0&single=true&output=csv'
    var lookupTable = {};

    // Function to fetch the lookup table data from the spreadsheet
    function fetchLookupTable(callback) {
        // Check if the lookup table data is already in local storage
        var storedLookupTable = localStorage.getItem('lookupTable');
        var storedTimestamp = localStorage.getItem('lookupTableTimestamp');
        var currentTimestamp = Date.now();
        if (storedLookupTable && storedTimestamp) {
            lookupTable = JSON.parse(storedLookupTable);
            var timeSinceLastUpdate = currentTimestamp - parseInt(storedTimestamp);
            var lookupTableRefreshTime = 7 * 24 * 60 * 60 * 1000; // 7 days in milliseconds. This can be updated to a longer cadence.
            if (timeSinceLastUpdate >= lookupTableRefreshTime) {
                // If it has been more than 7 days, fetch a new copy of the lookup table
                fetchTableFromUrl(callback, currentTimestamp);
            } else {
                // Use the stored lookup table
                callback(lookupTable);
            }
        } else {
            // If the lookup table data or timestamp is not in local storage, fetch it from the spreadsheet
            fetchTableFromUrl(callback, currentTimestamp);
        }
    }

    function fetchTableFromUrl(callback, currentTimestamp) {
        GM.xmlHttpRequest({
            method: 'GET',
            url: spreadsheetUrl,
            responseType: 'csv',
            onload: function(response) {
                if (response.status === 200) {
                    lookupTable = parseLookupTable(response.responseText);
                    // Store the lookup table data and the current timestamp in local storage
                    localStorage.setItem('lookupTable', JSON.stringify(lookupTable));
                    localStorage.setItem('lookupTableTimestamp', currentTimestamp.toString());
                    callback(lookupTable);
                } else {
                    console.error('Failed to fetch lookup table data.');
                }
            },
            onerror: function(error) {
                console.error('Failed to fetch lookup table data:', error);
            }
        });
    }

    // Function to parse the CSV lookup table data into an object
    function parseLookupTable(csvData) {
        var lines = csvData.split('\n');
        var lookupTable = {};
        var headers = lines[0].split(',');

        // For some reason, adding new headers to the spreadsheet messes up the formatting. Code below fixes the formatting by removing '\r'
        for (i=0; i < headers.length; i++) {
            if (headers[i].toString().indexOf('\r') > -1) {
                headers[i] = headers[i].replace('\r','').trim();
            }
        }

        for (var i = 1; i < lines.length; i++) {
            var parts = lines[i].split(',');
            var vendor = parts[0].trim();
            for (var j = 1; j < headers.length; j++) {
                var url = parts[j].trim();
                if (vendor && url) {
                    lookupTable[vendor] = lookupTable[vendor] || {}; // Create an empty object for the vendor if it doesn't exist
                    lookupTable[vendor][headers[j]] = url; // Store the URL for the corresponding column header
                }
            }
        }
        return lookupTable;
    }

    // Function to create the button
    function createButton(lookupTable) {
        // Selector for the existing description box and button
        var parentElement = document.querySelector('[data-test-expanded]');
        var descriptionBox = parentElement.querySelector('div.contextSectionContent > div.contextSectionInfo');
        var existingButton = parentElement.querySelector('div.contextSectionContent > button');

        // Determine the vendor and retrieve the corresponding URL from the lookup table. The vendor is determined by the name in the "Vendor" column in TiQ.
        var vendor = document.querySelector('[data-test-expanded]').firstElementChild.lastElementChild.querySelector('.cth-cell__type').innerText;
        var urls = lookupTable[vendor];

        if (descriptionBox && urls) {
            for (var column in urls) {
                if (urls.hasOwnProperty(column)) {
                    var button = document.createElement('button');
                    button.textContent = column;
                    button.classList.add('vendor-button'); // CSS class defined at end of code
                    button.addEventListener('click', function() {
                        event.stopPropagation();
                        window.open(urls[event.target.innerText], '_blank');
                    });

                    // Insert the button above the description box if it doesn't already exist
                    if (descriptionBox && urls[column] && !existingButton) {
                        descriptionBox.parentNode.insertBefore(button, descriptionBox);
                    }
                }
            }
        }
    }

    // CSS styles for the button
    var style = document.createElement('style');
    style.textContent = `
        .vendor-button {
            display: block;
            margin: 0 auto 10px;
            line-height: 28px !important;
            font-weight: 700;
            color: rgb(20, 125, 192);
            background-attachment: scroll;
            background-clip: border-box;
            background-color: rgb(255, 255, 255);
            background-image: none;
            background-origin: padding-box;
            background-size: auto;
            border-bottom-color: rgba(218, 226, 235, 0.66);
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgba(218, 226, 235, 0.66);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgba(218, 226, 235, 0.66);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgba(218, 226, 235, 0.66);
            border-top-style: solid;
            border-top-width: 1px;
            cursor: pointer;
            outline-color: rgb(20, 125, 192);
            outline-style: none;
            outline-width: 0px;
        }
        .vendor-button:hover {
            background-color: rgb(72, 169, 230);
            color: rgb(255, 255, 255);
        }
    `;
    document.head.appendChild(style);

    document.addEventListener('click', function() {
        // Check if we are on the "Tags" section in TiQ
        if (document.querySelector('#breadcrumb-container > div > div > div > span.breadcrumbs-level2.as-header-pagename.js-screen-name').innerText == 'Tags') {
            var parentElement = document.querySelector('[data-test-expanded]') || "";
            // Check if we have a tag expanded
            if (parentElement){
                var childElement = parentElement.querySelector('div.contextSectionContent > button') || "";
                // Check if the button doesn't already exist
                if (!childElement){
                    // Call the function to fetch the lookup table and create the button
                    fetchLookupTable(createButton);
                }
            }
        };
    })
})();
