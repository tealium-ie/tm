// ==UserScript==
// @name         CDH Vendor Documentation Button
// @author       Alex Vo
// @namespace    http://tampermonkey.net/
// @version      1.2
// @description  Inserts 'Gotcha' documentation when adding a new connector
// @match        https://sso.tealiumiq.com/tms*?*account=*
// @grant        GM.xmlHttpRequest
// ==/UserScript==
(function() {
    'use strict';

    // Lookup table for vendor documentation stored in Google Sheets
    const spreadsheetUrl = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT4vpgFpXOzVyY1P4OQjRRn3rYJ0grWE0Sy0PzH7EQLC3NXQe-M-te5T3mEZggjsOfEQtRQKYyZ7QJm/pub?gid=0&single=true&output=csv'
    var lookupTable = {};

    // Function to fetch the lookup table data from the spreadsheet
    function fetchLookupTable() {
        // Check if the lookup table data is already in local storage
        var storedLookupTable = localStorage.getItem('CDHlookupTable');
        var storedTimestamp = localStorage.getItem('CDHlookupTableTimestamp');
        var currentTimestamp = Date.now();
        if (storedLookupTable && storedTimestamp) {
            lookupTable = JSON.parse(storedLookupTable);
            var timeSinceLastUpdate = currentTimestamp - parseInt(storedTimestamp);
            var lookupTableRefreshTime = 7 * 24 * 60 * 60 * 1000; // 7 days in milliseconds. This can be updated to a longer cadence.
            if (timeSinceLastUpdate >= lookupTableRefreshTime) {
                // If it has been more than 7 days, fetch a new copy of the lookup table
                fetchTableFromUrl(currentTimestamp);
            }
        } else {
            // If the lookup table data or timestamp is not in local storage, fetch it from the spreadsheet
            fetchTableFromUrl(currentTimestamp);
        }
    }

    function fetchTableFromUrl(currentTimestamp) {
        GM.xmlHttpRequest({
            method: 'GET',
            url: spreadsheetUrl,
            responseType: 'csv',
            onload: function(response) {
                if (response.status === 200) {
                    lookupTable = parseLookupTable(response.responseText);
                    // Store the lookup table data and the current timestamp in local storage
                    localStorage.setItem('CDHlookupTable', JSON.stringify(lookupTable));
                    localStorage.setItem('CDHlookupTableTimestamp', currentTimestamp.toString());
                } else {
                    console.error('Failed to fetch lookup table data.');
                }
            },
            onerror: function(error) {
                console.error('Failed to fetch lookup table data:', error);
            }
        });
    }

    // Function to parse the CSV lookup table data into an object
    function parseLookupTable(csvData) {
        var lines = csvData.split('\n');
        var lookupTable = {};
        var headers = lines[0].split(',');

        // For some reason, adding new headers to the spreadsheet messes up the formatting. Code below fixes the formatting by removing '\r'
        for (i = 0; i < headers.length; i++) {
            if (headers[i].toString().indexOf('\r') > -1) {
                headers[i] = headers[i].replace('\r', '').trim();
            }
        }

        for (var i = 1; i < lines.length; i++) {
            var parts = lines[i].split(',');
            var vendor = parts[0].trim();
            for (var j = 1; j < headers.length; j++) {
                var url = parts[j].trim();
                if (vendor && url) {
                    lookupTable[vendor] = lookupTable[vendor] || {}; // Create an empty object for the vendor if it doesn't exist
                    lookupTable[vendor][headers[j]] = url; // Store the URL for the corresponding column header
                }
            }
        }
        return lookupTable;
    }

    // Function to create the button
    function addGotchaDocsTile(lookupTable, displayName) {
        // Check to ensure the vendor exists in the lookup table
        if (lookupTable[displayName]) {
            // Create a MutationObserver to monitor changes in the DOM
            var observer = new MutationObserver(function(mutationsList) {
                for (var mutation of mutationsList) {
                    if (mutation.type === 'childList') {
                        // Check if the existing div is available
                        var existingDiv = document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD')[document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD').length - 1];
                        if (existingDiv) {
                            // Check if the existing div is available
                            var existingLinks = existingDiv.querySelectorAll('a');
                            var numExistingLinks = existingLinks.length;
                            var lookupLinks = Object.keys(lookupTable[displayName]);
                            var numLookupLinks = lookupLinks.length;
                            // Calculate the number of new divs needed
                            var numNewDivs = Math.ceil((numLookupLinks + numExistingLinks) / 2) - 1;

                            // Declare column, url, link vars
                            var column, url, link;

                            // Add the first link from the lookup table to the existing div
                            if (numExistingLinks < 2 && numLookupLinks > 0) {
                                column = lookupLinks[0];
                                url = lookupTable[displayName][column];
                                link = createLinkElement(column, url);
                                existingDiv.appendChild(link);
                                lookupLinks.splice(0, 1);
                                numLookupLinks--;
                            }

                            // Create new divs with links from the lookup table
                            for (var i = 0; i < numNewDivs; i++) {
                                var newDiv = document.createElement('div');
                                newDiv.setAttribute('data-test', 'panel_content_container');
                                newDiv.classList.add('info_panel__ContentContainer-sc-1p4u71o-2', 'hQxPlD');

                                // Add links to the new div
                                for (var j = i * 2; j < Math.min((i * 2) + 2, numLookupLinks); j++) {
                                    column = lookupLinks[j];
                                    url = lookupTable[displayName][column];
                                    link = createLinkElement(column, url);
                                    newDiv.appendChild(link);
                                }

                                // Redefine the existingDiv and insert the new div after the existing div
                                existingDiv = document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD')[document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD').length - 1];
                                existingDiv.parentNode.insertBefore(newDiv, existingDiv.nextSibling);
                            }

                            // Disconnect the observer to stop monitoring
                            observer.disconnect();
                            break;
                        }
                    }
                }
            })
            // Start observing changes in the document body
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        }
    }

    // Helper function to create a link element
    function createLinkElement(column, url) {
        var link = document.createElement('a');
        link.classList.add('resource_link__Link-sc-1rmsw1q-0', 'AupFi');
        link.href = url;
        link.target = '_blank';
        link.dataset.test = 'vendor_docs_link';
        link.innerHTML = '<img src="/datacloud/static/img/vendor_docs_grey.svg" alt="Documentation image." class="resource_link__DocumentImg-sc-1rmsw1q-1 dLEIvZ"><span class="resource_link__Title-sc-1rmsw1q-2 YERGk">' + column + '</span><span class="resource_link__Description-sc-1rmsw1q-3 kQohas">Tampermonkey</span>';
        return link;
    }

    // Function to add MutationObserver used to add gotcha docs to the 'action' page
    function watchForActionPg() {
        // Create a MutationObserver to monitor changes in the DOM
        var observer1 = new MutationObserver(function(mutationsList) {
            for (var mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    // Check if the existing div is available. This div indicates we've reached the "Action" page
                    var existingDiv = document.querySelector('div.action-select');
                    if (existingDiv) {
                        // Run the addGotchaDocsList function to create the gotcha docs list
                        if (!document.querySelector('#gotchaDocsList')) {
                            addGotchaDocsList(lookupTable);
                        }
                    }
                    if (mutation.removedNodes && mutation.removedNodes.length > 0) {
                        for (var i = 0; i < mutation.removedNodes.length; i++) {
                            if (mutation.removedNodes[i].id == 'modal_manager_portal_1') {
                                observer1.disconnect();
                                console.log('Observer disconnected');
                                break;
                            }
                        }
                    }
                }
            }
        })
        // Start observing changes in the document body
        observer1.observe(document.body, {
            childList: true,
            subtree: true
        });
    }

    function addGotchaDocsList(lookupTable) {
        //Capture the connector name being added or updated
        var displayName = document.querySelector('div[data-test="default_modal_title"]');
        if (displayName) {
            displayName = displayName.textContent.replace(/^Add Action - /, '').replace(/^Edit Action - /, '');
        }
        if (lookupTable[displayName]) {
            var lookupLinks = Object.keys(lookupTable[displayName]);
            var numLookupLinks = lookupLinks.length;

            // Create an unordered list element to hold the gotcha docs.
            const ul = document.createElement('ul');
            ul.id = 'gotchaDocsList';
            var column, url;
            for (var i = 0; i < numLookupLinks; i++) {
                column = lookupLinks[i];
                url = lookupTable[displayName][column];
                // Create a list item for each gotcha doc.
                const li = document.createElement('li');
                const a = document.createElement('a');
                a.href = url;
                a.textContent = column;
                a.target = '_blank';
                li.appendChild(a);

                // Append the list item to the unordered list.
                ul.appendChild(li);
            }

            //  Set the styling of the gotcha doc list
            ul.style.display = 'block';
            ul.style.marginTop = '10px';

            // Add list to the Action page
            var targetElement = document.querySelector('div.action-select');
            if (targetElement) {
                targetElement.appendChild(ul);
            }
        }

    }

    //debugger;
    document.addEventListener('click', function(event) {
        // Check if we clicked on the "Add Connector", "Add Action", or "Edit (Action)" button to update the lookup table
        var addConnectorButton = document.querySelector('button[data-test="add-connector-button"]');
        var addActionButtons = document.querySelectorAll('button[data-test="add-action-button"]');
        var editActionButton = document.querySelector('button[data-test="edit_action_btn"]');
        if (event.target === addConnectorButton || event.target.parentElement === addConnectorButton) {
            fetchLookupTable();
        }
        //Clicking any "Add Action" button
        else if (Array.from(addActionButtons).includes(event.target) || Array.from(addActionButtons).includes(event.target.parentElement)) {
            fetchLookupTable();
            console.log('Start observer');
            watchForActionPg();
        }
        //Clicking the "Edit" button on an existing action
        else if (event.target === editActionButton || event.target.parentElement === editActionButton) {
            fetchLookupTable();
            console.log('Start observer');
            watchForActionPg();
        }
        //Clicking "Continue" from the Connector Marketplace
        else if (event.target && event.target.tagName === 'BUTTON' && event.target.hasAttribute('data-track-action')) {
            if (event.target.getAttribute('data-track-action').includes('connectormarketplace-continue')) {
                console.log('Start observer');
                setTimeout(watchForActionPg, 2000); //prevent the observer from disconnecting too early
            }
        }
        // Check if we clicked on a connector tile in the Connector Marketplace window
        else if (event.target.closest('.connector_tile__Tile-sc-15ne86l-2.djzzeS')) {
            // Grab the connector name that was clicked on
            var displayNameElement = event.target.closest('.connector_tile__Tile-sc-15ne86l-2.djzzeS').querySelector('.connector_tile__DisplayName-sc-15ne86l-0.ihfopP');
            var displayName = displayNameElement.textContent;
            addGotchaDocsTile(lookupTable, displayName);
        }
    });

    function createSettingsUI() {
        // Create a container for your settings overlay
        const settingsOverlay = document.createElement('div');
        settingsOverlay.id = 'settings-overlay';
        settingsOverlay.style.position = 'fixed';
        settingsOverlay.style.top = '0';
        settingsOverlay.style.left = '0';
        settingsOverlay.style.width = '100%';
        settingsOverlay.style.height = '100%';
        settingsOverlay.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; // Semi-transparent background
        settingsOverlay.style.zIndex = '9999'; // Ensure the overlay is on top

        // Create a container for the settings content
        const settingsContainer = document.createElement('div');
        settingsContainer.id = 'settings-container';
        settingsContainer.style.position = 'absolute';
        settingsContainer.style.top = '50%';
        settingsContainer.style.left = '50%';
        settingsContainer.style.transform = 'translate(-50%, -50%)';
        settingsContainer.style.backgroundColor = '#fff'; // White background
        settingsContainer.style.padding = '25px';
        settingsContainer.style.borderRadius = '8px';

        // Create hardcoded link to lookuptable
        const lookupLink = document.createElement('a');
        lookupLink.href = 'https://docs.google.com/spreadsheets/d/125F5BIj7d3Oq0A5eaVC6X3e4bJwXeC7YzfBKQUUoEQ4/edit#gid=0'; // Set the URL for the first link
        lookupLink.textContent = 'Lookup Table';
        lookupLink.target = '_blank';
        lookupLink.style.display = 'block';
        lookupLink.style.marginTop = '20px';
        lookupLink.style.color = '147dc0';
        settingsContainer.appendChild(lookupLink);

        // Create a close button
        const closeButton = document.createElement('button');
        closeButton.innerHTML = 'X';
        closeButton.style.position = 'absolute';
        closeButton.style.top = '10px';
        closeButton.style.right = '10px';
        closeButton.style.cursor = 'pointer';
        // Add a click event listener to close the overlay when the "X" button is clicked
        closeButton.addEventListener('click', closeSettingsOverlay);

        // Create a button to clear localStorage
        const clearStorageButton = document.createElement('button');
        clearStorageButton.innerHTML = 'Refresh Gotcha Doc Lookup Table';
        clearStorageButton.style.backgroundColor = '#147DC0';
        clearStorageButton.style.color = '#fff';
        clearStorageButton.style.border = '1px';
        clearStorageButton.style.padding = '0px 8px';
        clearStorageButton.style.marginTop = '20px';
        clearStorageButton.style.cursor = 'pointer';
        clearStorageButton.style.borderRadius = '4px';
        // Add a click event listener to clear localStorage and show a success message
        clearStorageButton.addEventListener('click', function() {
            var currentTimestamp = Date.now();
            const successMessage = document.createElement('div');
            fetchTableFromUrl(currentTimestamp)
            setTimeout(function() {
                if (localStorage.getItem('CDHlookupTableTimestamp') == currentTimestamp) {
                    successMessage.textContent = 'Successful Update!';
                    successMessage.style.color = '#4CAF50'; // Green text color for success
                    settingsContainer.appendChild(successMessage);
                } else {
                    successMessage.textContent = 'Something went wrong!';
                    successMessage.style.color = '#f44336'; // Red text color for failure
                    settingsContainer.appendChild(successMessage);
                }
            }, 2000);
        });

        // Add the UI elements to the settings container
        settingsContainer.appendChild(closeButton);
        settingsContainer.appendChild(clearStorageButton);

        // Add the settings container to the overlay
        settingsOverlay.appendChild(settingsContainer);

        // Append the overlay to the body
        document.body.appendChild(settingsOverlay);
    }

    // Function to close the settings overlay
    function closeSettingsOverlay() {
        const settingsOverlay = document.getElementById('settings-overlay');
        const menuTargetElement = document.querySelector("li.toplvl-menu-list-item[data-section='tools'] > ul.submenu-list-menu[data-test='submenu-list']");
        if (settingsOverlay) {
            settingsOverlay.remove();
            //Update styling to ensure selected menu item remains highlighted
            document.getElementById('tm-gotcha-doc').classList.remove('active')
            if (window.location.hash.includes('#visitor-profile-sampler')) {
                menuTargetElement.querySelector("li[data-route='visitor-profile-sampler']").classList.add('active');
            } else if (window.location.hash.includes('#rules')) {
                menuTargetElement.querySelector("li[data-route='rules']").classList.add('active');
            } else if (window.location.hash.includes('#visitor-lookup/json')) {
                menuTargetElement.querySelector("li[data-route='visitor-lookup/json']").classList.add('active');
            }
        }
    }

    // Function to check if the menuTargetElement is available
    function checkMenuTargetElement() {
        const menuTargetElement = document.querySelector("li.toplvl-menu-list-item[data-section='tools'] > ul.submenu-list-menu[data-test='submenu-list']");

        // Check if the menuTargetElement is found
        if (menuTargetElement) {
            // Create the "Gotcha Docs" menu item
            const newListItem = document.createElement("li");
            newListItem.className = "submenu-list-item js-submenu-item";
            newListItem.id = "tm-gotcha-doc";
            newListItem.textContent = "Gotcha Docs";
            newListItem.addEventListener("click", createSettingsUI);
            menuTargetElement.appendChild(newListItem);

            // Clear the interval (stop polling) once the element is found
            clearInterval(menuCheckInterval);
        }
    }

    // Initialize a counter and maximum attempts
    let attempts = 0;
    const maxAttempts = 12;

    // Poll for the menuTargetElement every 5 seconds (5000 milliseconds)
    const menuCheckInterval = setInterval(function() {
        attempts++;

        // Check if the maximum number of attempts has been reached
        if (attempts >= maxAttempts) {
            // Stop polling and handle the case where the element was not found
            clearInterval(menuCheckInterval);
            console.log("MenuTargetElement not found after maximum attempts.");
        } else {
            // Check if the menuTargetElement is available
            checkMenuTargetElement();
        }
    }, 5000); // Poll every 5 seconds
})();
